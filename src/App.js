import React from 'react';

import { Global, css } from '@emotion/core';
import styled from '@emotion/styled';
import { ThemeProvider } from 'emotion-theming';
import { compose, withState, withPropsOnChange } from 'recompose';
import ReactTooltip from 'react-tooltip';

import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import { createBrowserHistory } from 'history';

import content from './produced_items.json';
import { MCText, MCItem, MCCraftingTable, Wrapper } from './ui-components';
import GlobalStyle from './components/GlobalStyle';
import { MinecraftSearch } from './components';

import configureStore from './configureStore';


import { Route, Switch } from 'react-router';
import { connect } from 'react-redux';
import { createSink } from 'recompose';
import { push } from 'connected-react-router';

const history = createBrowserHistory();
const store = configureStore({}, history);

const TestComponent = compose(
  connect(({ router }) => ({
    router,
  }), dispatch => ({ push: (route) => dispatch(push(route)) })),
)(({ router, push }) => {
  console.log(router);
  window.updateRoute = (route) => {
    console.log(route);
    const result = push(route);
    window.result = result;
    console.log(result);
  };
  return null;
});

function App({ theme, setTheme }) {
  return (
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <ThemeProvider theme={theme}>
          <GlobalStyle />
          <Wrapper>
            <Switch>
              <Route exact path="/" render={() => <div>We are here!</div>} />
              <Route render={() => (<div>Miss</div>)} />
            </Switch>
            <TestComponent />
            
            <h1>Minecraft Enchantment Tool</h1>
            <MinecraftSearch searchableContent={content} />
            {/* <input type="number" value={theme.scale} onChange={({target: {value}}) => setTheme({ scale: value })} /> */}
            <ReactTooltip place="bottom" type="dark" effect="float"/>
          </Wrapper>
        </ThemeProvider>
      </ConnectedRouter>
    </Provider>
  );
}

export default withState('theme', 'setTheme', { scale: 1 })(App);
