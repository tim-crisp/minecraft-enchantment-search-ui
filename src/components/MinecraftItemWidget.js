import React from 'react';
import PropTypes from 'prop-types';
import Fuse from 'fuse.js';
import * as R from 'ramda';
import ReactTooltip from 'react-tooltip';
import { compose, withState, withPropsOnChange } from 'recompose';
import styled from '@emotion/styled';
import { fuseOptions } from '../config';
import { MCItem, MCText, MCCraftingTable } from '../ui-components';

const Spacer = styled.hr`
  margin: 20px 0;
`;

const MinecraftItemWidget = ({ item }) => (
  <div>
    <Spacer />
    <MCItem item={item} />
    <MCCraftingTable item={item} />
    Name: {item.title}
    Description: {item.paragraph}
  </div>
);

MinecraftItemWidget.propTypes = {
  item: PropTypes.object,
};

export default MinecraftItemWidget;
