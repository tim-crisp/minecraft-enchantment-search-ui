import React from 'react';
import Fuse from 'fuse.js';
import ReactTooltip from 'react-tooltip';
import { compose: composeRecompose, withState, withPropsOnChange, createSink, lifecycle } from 'recompose';
import styled from '@emotion/styled';
import { applySpec, pathOr } from 'react';
import { fuseOptions } from '../config';
import { MCItem, MCText, MCCraftingTable } from '../ui-components';
import MinecraftItemWidget from './MinecraftItemWidget';
import { connect } from 'react-redux';

const SearchResultTotal = styled.div`
  float:right;
`;

const mapStateToProps = applySpec({
  searchableContent: path(['mcSearch', 'searchableContent']),
  searchResults: path(['mcSearch', 'searchresults']),
});

const SearchComponent = composeRecompose(
  // connect(

  // )
  withState('searchResults', 'setSearchResults', []),
  withPropsOnChange(
    ['searchableContent'],
    ({ searchableContent, setSearchResults }) => {
      const _searchableContent = new Fuse(searchableContent, fuseOptions);
      return {
        searchList: query => setSearchResults(
          // R.groupBy(
          //   R.ifElse(
          //     R.has('parentType'),
          //     R.prop('parentType'),
          //     R.prop('title'),
          //   )
          // )
          (
            _searchableContent.search(query)
          )
        ),
      }
    }
  ),
  lifecycle({
    componentDidUpdate: ReactTooltip.rebuild,
  }),
)(({
  searchResults,
  searchList,
}) => (
  <div>
    <MCText onChange={({ target: { value }}) => searchList(value)} />
    <SearchResultTotal>Total Items: {searchResults.length}</SearchResultTotal>
    {searchResults.length > 0 && (
      searchResults.map(item => (
        <MinecraftItemWidget key={item.title} item={item} />
      ))
    )}
  </div>
));

export default SearchComponent;
