import React from 'react';
import { Global, css } from '@emotion/core';

export default () => (
<Global
  styles={css`
    @font-face {
      font-family: 'Minecraft';
      src: url('mcfont.eot');
      src: url('mcfont.eot?#iefix') format('embedded-opentype'),
          url('mcfont.woff') format('woff'),
          url('mcfont.ttf')  format('truetype'),
          url('mcfont.svg#svgFontName') format('svg');
    }
    body {
      background-image: url(bg.png);
      font-family: Minecraft, sans-serif;
    }
    
  `}
/>
);
