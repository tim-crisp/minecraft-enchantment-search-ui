
export const fuseOptions = {
  shouldSort: true,
  findAllMatches: true,
  threshold: 0.6,
  location: 0,
  distance: 100,
  maxPatternLength: 32,
  minMatchCharLength: 1,
  keys: [
    "paragraph",
    "title",
    "craftingResultItems.title"
  ]
};

