/**
 * Create the store with dynamic reducers
 */

import { createStore, applyMiddleware, compose } from 'redux';
import { routerMiddleware } from 'connected-react-router';
import createReducer from './reducers';

import Fuse from 'fuse.js';

const searchMiddleware = ({ getState, dispatch }) => {
  let searchInstance;
  return next => action => {

    const returnValue = next(action);

    console.log(returnValue);
    console.log('we done something ', getState());

    return returnValue;
  };
};

export default function configureStore(initialState = {}, history) {
  const middlewares = [searchMiddleware, routerMiddleware(history)];

  const enhancers = [applyMiddleware(...middlewares)];

  const composeEnhancers = process.env.NODE_ENV !== 'production' && typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
      shouldHotReload: false
    })
    : compose;
  /* eslint-enable */



  const store = createStore(createReducer({}, history), initialState, composeEnhancers(...enhancers));

  store.injectedReducers = {}; // Reducer registry

  // Make reducers hot reloadable, see http://mxs.is/googmo
  /* istanbul ignore next */
  if (module.hot) {
    module.hot.accept('./reducers', () => {
      store.replaceReducer(createReducer(store.injectedReducers, history));
      store.dispatch({ type: '@@REDUCER_INJECTED' });
    });
  }

  return store;
}