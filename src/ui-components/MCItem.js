/** @jsx jsx */ 
import React from 'react';
import { __, converge, concat, path, compose, tap, prop, ifElse, has, when, unless, isNil } from 'ramda';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';
import { css, jsx } from '@emotion/core';
import { compose as recomposeCompose } from 'recompose';

const getItemCoordinates = compose(
  unless(
    isNil,
    converge(concat, [
      compose(concat(__, 'px '), prop('x')),
      compose(concat(__, 'px'), prop('y')),
    ]),
  ),
  prop('image'),
  when(
    has('craftingResultItem'),
    prop('craftingResultItem'),
  ),
  prop('item')
);

const MCItemWrapper = styled.div`
  background-image: url(mcitem-bg.png);
  padding: 1px;
  height: ${props => props.theme.scale * 34}px;
  width: ${props => props.theme.scale * 34}px;
  &:hover {
    background-image: url(mcitem-bg-hover.png);
  }
`;

const MCItemImage = styled.div`
  background: url(InvSprite.png);
  display: block;
  height: 32px;
  width: 32px;
`;

const MCItem = (props) => (
  <MCItemWrapper {...props}>
    {props.item && <MCItemImage data-tip={props.item.title} css={css`background-position: ${getItemCoordinates(props)};`} />}
  </MCItemWrapper>
);

MCItem.propTypes = {
  item: PropTypes.shape({
    title: PropTypes.string.isRequired,
    image: PropTypes.shape({
      x: PropTypes.string,
      y: PropTypes.string,
    })
  }),
};
MCItem.defaultProps = {
  item: {},
};

export default MCItem;
