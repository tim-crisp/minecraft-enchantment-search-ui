import styled from '@emotion/styled';

export default styled.div`
  background-color: #c6c6c6;
  margin: auto;
  max-width: 500px;
  min-width: 500px;
  padding: 30px 30px;

  image-rendering: -webkit-crisp-edges;
  image-rendering: -moz-crisp-edges;
  image-rendering: crisp-edges;
  image-rendering: pixelated;
  -ms-interpolation-mode: nearest-neighbor;
`;
