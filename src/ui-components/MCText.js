import styled from '@emotion/styled';

export default styled.input`
  font-family: 'Minecraft';
  border: none;
  background: none;
  background-image: url(input.png);
  outline: none;
  padding-left: 10px;
  color: white;
  background-size: ${props => props.theme.scale * 170}px;
  height: ${props => props.theme.scale * 21}px;
  width: ${props => props.theme.scale * 170 - 10}px;
  font-size: ${props => props.theme.scale * 16}px;
  text-shadow: ${props => props.theme.scale * 2}px ${props => props.theme.scale * 2}px #3f3f3f;
`;
