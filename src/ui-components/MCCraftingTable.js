/** @jsx jsx */ 
import React from 'react';
import { __, converge, concat, path, compose, tap, prop, ifElse, has, when, unless, isNil } from 'ramda';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';
import { css, jsx } from '@emotion/core';
import MCItem from './MCItem';

const MCCraftingTableWrapper = styled.div`
  display: flex;
  display: grid;
  grid-template-columns: min-content min-content min-content;
  grid-template-rows: min-content min-content min-content;
`;

const MCCraftingTable = (props) => (
  <MCCraftingTableWrapper>
    {props.item && props.item.craftingRecipe && props.item.craftingRecipe.map(item => (
      <MCItem item={item} />
    ))}
  </MCCraftingTableWrapper>
);


export default MCCraftingTable;
